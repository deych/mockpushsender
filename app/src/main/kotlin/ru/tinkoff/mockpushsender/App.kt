package ru.tinkoff.mockpushsender

import android.app.Application
import ru.tinkoff.mockpushsender.model.repo.Repository

/**
 * @author Alexander Deych
 */
class App : Application() {

    override fun onCreate() {
        super.onCreate()
        Repository.init(this)
    }
}