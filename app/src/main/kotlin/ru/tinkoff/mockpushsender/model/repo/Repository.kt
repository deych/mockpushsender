package ru.tinkoff.mockpushsender.model.repo

import android.content.Context
import com.google.gson.Gson
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import ru.tinkoff.mockpushsender.R
import ru.tinkoff.mockpushsender.model.entity.PushData
import ru.tinkoff.mockpushsender.model.entity.PushInfo
import java.io.InputStreamReader
import java.util.*

/**
 * @author Alexander Deych
 */
object Repository {

    lateinit private var context: Context
    lateinit private var storage: Storage<PushInfo>
    private val data = ArrayList<PushInfo>()
    private val gson = Gson()
    private val fromRaw by lazy {
        listOf(
                PushInfo("Маркетинговые пуши - Спецпредложения.\n\nОбщий список спецпредложений.",
                        readJson(R.raw.loyaltyoffer_all)),
                PushInfo("Маркетинговые пуши - Спецпредложения.\n\nКонкретное спецпредложение с подробными условиями акций (offerId)",
                        readJson(R.raw.loyaltyoffer_offerid)),

                PushInfo("Маркетинговые пуши - Оплата.\n\nЭкран оплаты провайдера MTS",
                        readJson(R.raw.pay_provider)),
                PushInfo("Маркетинговые пуши - Оплата.\n\nКонкретная группа провайдеров в виде списка (Интернет).",
                        readJson(R.raw.pay_group)),
                PushInfo("Маркетинговые пуши - Оплата.\n\nПополнение конкретной карты с карты другого банка.",
                        readJson(R.raw.pay_c2c_in_new)),

                PushInfo("Маркетинговые пуши - Партнёры.\n\nКарта с банкоматами и точками оплаты.",
                        readJson(R.raw.depositionpartners_atms)),
                PushInfo("Маркетинговые пуши - Партнёры.\n\nКарта с конкретным выбранным партнером для оплаты (MTS).",
                        readJson(R.raw.depositionpartners_mts)),

                PushInfo("Маркетинговые пуши - Лимиты.\n\nБезопасность и лимиты.",
                        readJson(R.raw.security_limits_screen)),
                PushInfo("Маркетинговые пуши - Лимиты.\n\nЛимиты по картам.",
                        readJson(R.raw.security_card_limit)),
                PushInfo("Маркетинговые пуши - Лимиты.\n\nЛимиты для МБ и ИБ.",
                        readJson(R.raw.security_limits_mb)),

                PushInfo("Маркетинговые пуши - Профиль.\n\nИзменение текущего email.",
                        readJson(R.raw.profile_edit_email)),
                PushInfo("Маркетинговые пуши - Профиль.\n\nДобавление нового email.",
                        readJson(R.raw.profile_add_email)),
                PushInfo("Маркетинговые пуши - Профиль.\n\nЭкран Профиль.",
                        readJson(R.raw.profile_screen)),

                PushInfo("Маркетинговые пуши - Новый продукт.\n\nСоздание накопительного счета / цели.",
                        readJson(R.raw.newproduct_saving)),
                PushInfo("Маркетинговые пуши - Новый продукт.\n\nОформление дополнительной карты (для конкретного продукта клиента).",
                        readJson(R.raw.newproduct_additional_card)),
                PushInfo("Маркетинговые пуши - Новый продукт.\n\nОформление вклада.",
                        readJson(R.raw.newproduct_deposit)),

                PushInfo("Маркетинговые пуши - Реклама.\n\nПриведи друга.",
                        readJson(R.raw.ads_friend)),

                PushInfo("Маркетинговые пуши - Прочее.\n\nВозможность открыть внешний URL во встроенном в приложение веб-вьюере.",
                        readJson(R.raw.other_url)),

                PushInfo("Маркетинговые пуши - Подписки.\n\nПо подписке появился новый счёт на оплату",
                        readJson(R.raw.subscription_new))
        )
    }

    fun init(context: Context) {
        this.context = context.applicationContext
        storage = PrefStorage(this.context)
    }

    fun loadData(listener: (data: List<PushInfo>) -> Unit) {
        doAsync {
            data.clear()
            data.addAll(storage.reload())
            if (data.isEmpty()) {
                data.addAll(loadFromRaw())
            }
            uiThread {
                listener.invoke(data)
            }
        }
    }

    fun saveData() {
        doAsync {
            storage.persist()
        }
    }

    fun resetData(listener: (data: List<PushInfo>) -> Unit) {
        doAsync {
            storage.clear()
            data.clear()
            data.addAll(loadFromRaw())
            uiThread {
                listener.invoke(data)
            }
        }
    }

    fun editData(position: Int, item: PushInfo) {
        doAsync {
            storage.update(position, item)
            storage.persist()
        }
    }

    fun addData(item: PushInfo) {
        doAsync {
            storage.add(item)
            storage.persist()
        }
    }

    fun removeData(position: Int) {
        doAsync {
            storage.remove(position)
            storage.persist()
        }
    }

    fun getData(position: Int): PushInfo = data[position]

    private fun loadFromRaw(): MutableList<PushInfo> {
        storage.addAll(fromRaw)
        storage.persist()
        return storage.reload()
    }

    private fun readJson(resId: Int): PushData {
        return gson.fromJson(InputStreamReader(context.resources.openRawResource(resId)), PushData::class.java)
    }
}