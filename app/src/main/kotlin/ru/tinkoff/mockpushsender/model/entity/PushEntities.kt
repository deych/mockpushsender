package ru.tinkoff.mockpushsender.model.entity

import android.support.annotation.ColorRes
import ru.tinkoff.mockpushsender.R
import java.util.*

/**
 * @author Alexander Deych
 */
data class PushInfo(var title: String, val content: PushData)

data class PushData(var resultCode: ResultCode, var payload: HashMap<String, String>)

enum class ResultCode(@ColorRes val color: Int) {
    LOYALTYOFFER(R.color.loyalty_offer),
    PAY(R.color.pay),
    DEPOSITIONPARTNERS(R.color.deposition_partners),
    SECURITY(R.color.security),
    PROFILE(R.color.profile),
    NEWPRODUCT(R.color.new_product),
    ADS(R.color.ads),
    OTHER(R.color.other),
    GEOLOCATION(R.color.other),
    SUBSCRIPTION(R.color.subscription);
}