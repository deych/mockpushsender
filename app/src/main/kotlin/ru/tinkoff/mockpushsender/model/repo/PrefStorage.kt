package ru.tinkoff.mockpushsender.model.repo

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.jetbrains.anko.defaultSharedPreferences
import ru.tinkoff.mockpushsender.model.entity.PushInfo
import java.util.*

private const val KEY_DATA = "data"

/**
 * @author Alexander Deych
 */
class PrefStorage(context: Context) : Storage<PushInfo> {

    private val prefs = context.defaultSharedPreferences
    private val items = ArrayList<PushInfo>()
    private val gson = Gson()

    override fun add(item: PushInfo) {
        items += item
    }

    override fun update(index: Int, item: PushInfo) {
        items[index] = item
    }

    override fun clear() = items.clear()

    override fun remove(index: Int) {
        items.removeAt(index)
    }

    override fun persist() = prefs.edit().putString(KEY_DATA, gson.toJson(items)).apply()

    override fun size(): Int = items.size

    override fun reload(): MutableList<PushInfo> {
        if (items.isNotEmpty()) {
            return items
        }
        val json = prefs.getString(KEY_DATA, null)
        val type = object : TypeToken<List<PushInfo>>() {}.type
        items.clear()
        if (json != null) {
            items.addAll(gson.fromJson<List<PushInfo>>(json, type))
        }
        return items
    }

    override fun addAll(collection: Collection<PushInfo>) {
        items.addAll(collection)
    }
}