package ru.tinkoff.mockpushsender.model.repo

/**
 * @author Alexander Deych
 */
interface Storage<T> {
    fun add(item: T)
    fun update(index: Int, item: T)
    fun clear()
    fun remove(index: Int)
    fun persist()
    fun size(): Int
    fun reload(): MutableList<T>
    fun addAll(collection: Collection<T>)
}