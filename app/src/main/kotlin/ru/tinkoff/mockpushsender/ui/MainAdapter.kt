package ru.tinkoff.mockpushsender.ui

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import org.jetbrains.anko.backgroundColor
import ru.tinkoff.mockpushsender.R
import ru.tinkoff.mockpushsender.model.entity.PushInfo
import java.util.*

/**
 * @author Alexander Deych
 */
class MainAdapter(private val context: Context,
                  private val editListener: (position: Int) -> Unit,
                  private val sendListener: (pushInfo: PushInfo) -> Unit) : RecyclerView.Adapter<ViewHolder>() {

    private val items: MutableList<PushInfo> = ArrayList()

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val info = items[position]
        holder?.title?.text = info.title
        holder?.itemView?.backgroundColor = ContextCompat.getColor(context, info.content.resultCode.color);
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder? {
        val view = LayoutInflater.from(context).inflate(R.layout.item_push_info, parent, false)
        return ViewHolder(view).listen({
            editListener.invoke(it)
        }, {
            sendListener.invoke(items[it])
        })
    }

    fun setData(items: List<PushInfo>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }
}

class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
    val title: TextView = itemView?.findViewById(R.id.title) as TextView
    val send: View = itemView?.findViewById(R.id.send) as View
}

fun ViewHolder.listen(editListener: (position: Int) -> Unit, sendListener: (position: Int) -> Unit): ViewHolder {
    itemView.setOnClickListener {
        editListener.invoke(adapterPosition)
    }
    send.setOnClickListener {
        sendListener.invoke(adapterPosition)
    }
    return this
}