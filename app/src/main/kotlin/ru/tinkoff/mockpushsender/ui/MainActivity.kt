package ru.tinkoff.mockpushsender.ui

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Base64
import android.view.Menu
import android.view.MenuItem
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.startActivityForResult
import ru.tinkoff.mockpushsender.R
import ru.tinkoff.mockpushsender.model.entity.PushInfo
import ru.tinkoff.mockpushsender.model.repo.Repository

const val PUSH_ACTION = "ru.tcsbank.mb.MOCK_PUSH"
const val EXTRA_PAYLOAD = "payload"
const val REQ_EDIT = 1001

class MainActivity : AppCompatActivity() {

    private val gson = Gson()
    private val adapter = MainAdapter(this, {
        startActivityForResult<EditActivity>(REQ_EDIT, EXTRA_POSITION to it)
    }, {
        processPushInfo(it)
    })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initViews()
    }

    override fun onResume() {
        super.onResume()
        loadData()
    }

    override fun onStop() {
        super.onStop()
        Repository.saveData()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_reset -> Repository.resetData { adapter.setData(it) }
            R.id.action_add -> startActivityForResult<EditActivity>(REQ_EDIT)
        }
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQ_EDIT && resultCode == RESULT_OK) {
            loadData()
        }
    }

    private fun loadData() {
        Repository.loadData {
            adapter.setData(it)
        }
    }

    private fun initViews() {
        list.layoutManager = LinearLayoutManager(this)
        list.adapter = adapter
    }

    private fun processPushInfo(info: PushInfo) {
        doAsync {
            val json = gson.toJson(info.content).toByteArray()
            sendBroadcast(Intent(PUSH_ACTION).putExtra(EXTRA_PAYLOAD, String(Base64.encode(json, Base64.DEFAULT))))
        }
    }
}
