package ru.tinkoff.mockpushsender.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import android.widget.ArrayAdapter
import com.google.gson.GsonBuilder
import com.google.gson.JsonSyntaxException
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_edit.*
import org.jetbrains.anko.toast
import ru.tinkoff.mockpushsender.R
import ru.tinkoff.mockpushsender.model.entity.PushData
import ru.tinkoff.mockpushsender.model.entity.PushInfo
import ru.tinkoff.mockpushsender.model.entity.ResultCode
import ru.tinkoff.mockpushsender.model.repo.Repository
import java.util.*

/**
 * @author Alexander Deych
 */

const val EXTRA_POSITION = "extra_position"

class EditActivity : AppCompatActivity() {

    var position: Int = -1
    lateinit var pushInfo: PushInfo
    val gson = GsonBuilder().setPrettyPrinting().create()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        position = intent.getIntExtra(EXTRA_POSITION, -1)
        pushInfo = if (position == -1) {
            PushInfo("", PushData(ResultCode.LOYALTYOFFER, HashMap()))
        } else {
            Repository.getData(position)
        }
        initViews()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.edit_menu, menu)
        menu?.findItem(R.id.action_delete)?.isVisible = position > -1
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_save -> save()
            R.id.action_delete -> delete()
            android.R.id.home -> finish()
        }
        return true
    }

    private fun delete() {
        Repository.removeData(position)
        setResult(RESULT_OK)
        finish()
    }

    private fun save() {
        pushInfo.title = etTitle.text.toString()
        pushInfo.content.resultCode = spType.selectedItem as ResultCode

        if (!TextUtils.isEmpty(etPayload.text.toString().trim())) {
            try {
                val type = object : TypeToken<HashMap<String, String>>() {}.type
                pushInfo.content.payload = gson.fromJson(etPayload.text.toString(), type)
            } catch (e: JsonSyntaxException) {
                toast("Can't parse JSON")
                e.printStackTrace()
                return
            }
        }

        if (position == -1) {
            Repository.addData(pushInfo)
        } else {
            Repository.editData(position, pushInfo)
        }
        setResult(RESULT_OK)
        finish()
    }

    private fun initViews() {
        etTitle.setText(pushInfo.title)
        title = if (TextUtils.isEmpty(pushInfo.title)) "New push" else pushInfo.title

        val adapter = ArrayAdapter<ResultCode>(this, android.R.layout.simple_spinner_item, ResultCode.values())
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spType.adapter = adapter
        spType.setSelection(pushInfo.content.resultCode.ordinal, false)

        if (pushInfo.content.payload != null) {
            etPayload.setText(gson.toJson(pushInfo.content.payload))
        }
    }
}